

<form id="contactForm" method="POST">
    <div class="row">
        <div class="col-12 col-md-6">
            <p>
                <label style="width: 100%"> Twoje imię<br />
                    <input type="text" name="name" id="name" value="" size="40" aria-required="true"
                           aria-invalid="false" placeholder="Wpisz swoje imię" />
                    <br />
                </label>
            </p>
        </div>
        <div class="col-12 col-md-6">
            <p><label style="width: 100%"> Twój email<br />
                    <input type="email" name="email" id="email" value="" size="40" aria-required="true"
                           aria-invalid="false" placeholder="Wpisz swój email" />
                    <br />
                </label>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <p>
                <label style="width: 100%"> Wiadomość<br />
                    <textarea name="message" id="message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea"
                              aria-invalid="false" placeholder="Wpisz swoją wiadomość"></textarea>
                    <br />
                </label>
            </p>
        </div>
    </div>

    <div style="margin-left: 15px; margin-right: 15px" class="form-group">
        <div class="form-check">
            <input id="rule" type="checkbox" name="rule" placeholder="Rule" class="form-check-input">
            <label for="rule" class="form-check-label">{!! getConstField('contact_form_rule') !!}</label>
            <div class="invalid-feedback"></div>
        </div>
    </div>

    <div style="margin-left: 15px; margin-right: 15px" class="form-group">
        <div class="g-recaptcha" data-sitekey="{{$siteKey}}"></div>
        <div class="invalid-feedback"></div>
    </div>

    <p style="margin-left: 15px; margin-right: 15px"><input type="submit" value="Wyślij" /></p>


    <div id="alert" class="alert"></div>
</form>



@push('scripts.body.bottom')
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <script>
        document.getElementById('contactForm').addEventListener('submit', e => {
            e.preventDefault();
            submitForm(e.target);
        })
    </script>
@endpush
