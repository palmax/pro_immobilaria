@if($items->count() > 0)
    <ul id="myNavbar" class="navbar_nav">
        @foreach($items as $item)
            @php
                $isActive = false;
                $url = null;
                $target = '_self';
                if($item->page) {
                    $url = route($item->page->type);
                }
                else {
                    $url = url()->to($item->url);
                }

                if($item->target) {
                    $target = $item->target;
                }

                $isActive = request()->fullUrlIs($url);
            @endphp
            @if($item->navItems->count() > 0)
            <li class="{{ $isActive ? 'active' : '' }} menu-item menu-item-has-children dropdown nav-item">
            @else
            <li class="{{ $isActive ? 'active' : '' }} menu-item nav-item">
            @endif
                @if($item->navItems->count() > 0)
                <a href="{{$url}}" class="dropdown-toggle nav-link">
                    @else
                        <a href="{{$url}}" class="dropdown-toggle nav-link">
                    @endif
                    <span>{{$item->label}}</span>
                </a>
                @if($item->navItems->count() > 0)
                <ul class="dropdown-menu" style="width: 100%;padding: 0px 25px;">
                    @foreach($item->navItems as $subItem)
                        @php
                            $isActive = false;
                            $url = null;
                            $target = '_self';
                            if($subItem->page) {
                                $url = route($subItem->page->type);
                            }
                            else {
                                $url = url()->to($subItem->url);
                            }

                            if($subItem->target) {
                                $target = $subItem->target;
                            }

                            $isActive = request()->fullUrlIs($url);
                        @endphp
                    <li class="menu-item  nav-item">
                        <a href="{{$url}}" style="font-size: 15px; font-family: var(--creote-family-one); font-weight: 600;line-height: 65px; color: var(--menu-color)!important;" class="dropdown-item nav-link">
                            <span>{{ $subItem->label }}</span>
                        </a>
                    </li>
                    @endforeach
                </ul>
                @endif
            </li>
        @endforeach
    </ul>
@endif
