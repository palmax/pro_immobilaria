<!doctype html>
<html lang="{{app()->getLocale()}}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    {!! SEOMeta::generate() !!}

    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('image/favicon/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('image/favicon/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('image/favicon/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('image/favicon/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('image/favicon/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('image/favicon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('image/favicon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('image/favicon/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('image/favicon/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('image/favicon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('image/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('image/favicon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('image/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('image/favicon//manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link rel='stylesheet'
          href='https://fonts.googleapis.com/css?family=Spartan%3A400%2C500%2C600%2C700%2C800%2C900%7CInter%3A300%2C400%2C500%2C600%2C700%2C800%2C900&subset=latin%2Clatin-ext'
          type='text/css' media='all' />

    <link href="{{asset('css/vendors/bootstrap.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link href="{{asset('css/main.css')}}?version=1" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simplelightbox/2.10.3/simple-lightbox.min.css" integrity="sha512-Ne9/ZPNVK3w3pBBX6xE86bNG295dJl4CHttrCp3WmxO+8NQ2Vn8FltNr6UsysA1vm7NE6hfCszbXe3D6FUNFsA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel='stylesheet' href='{{asset('css/bootstrap.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{asset('css/owl.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{asset('css/swiper.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{asset('css/jquery.fancybox.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{asset('css/icomoon.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{asset('css/flexslider.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{asset('css/font-awesome.min.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{asset('css/style.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' href='{{asset('css/scss/elements/theme-css.css')}}' type='text/css' media='all' />
    <link rel='stylesheet' id="creote-color-switcher-css" href='{{asset('assets/css/scss/elements/color-switcher/color.css')}}'
          type='text/css' media='all' />

    <script>
        const BASE_URL = '{{url()->to('/')}}/';
        const CSRF_TOKEN = '{{csrf_token()}}';
        const SITE_LANG = '{{app()->getLocale()}}';
    </script>

    @stack('scrips.head.bottom')
</head>
<body>
{{--@include('default._helpers.lang_nav')--}}
<body class="home theme-creote page-home-default-one">
<div id="page" class="page_wapper hfeed site">
    <div style="display: flex; flex-direction: row" id="wrapper_full" class="content_all_warpper">
        <!----pre loader----->
        <div class="preloader-wrap">
            <div class="preloader" style="background-image:url({{asset('images/preloader.gif')}})">
            </div>
            <div class="overlay"></div>
        </div>
        <!----pre loader end----->
        <!----header----->
        <div class="header_area " id="header_contents">
            <header class="header header_default style_two ">
                <div class="auto-container">
                    <div class="row align-items-center" style="flex-direction: column">
                        <div class="col-lg-12 logo_column">
                            <div class="header_log_outer">
                                <div class="header_logo_box">
                                    <a href="/" class="logo navbar-brand">
                                        <img src="{{asset('images/logo.png')}}" class="logo_default">
                                        <img src="{{asset('images/logo.png')}}" class="logo__sticky">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 menu_column">
                            <div class="navbar_togglers hamburger_menu">
                                <span class="line"></span>
                                <span class="line"></span>
                                <span class="line"></span>
                            </div>
                            <div class="header_content_collapse" style="margin-top: 30px">
                                <div class="header_menu_box">
                                    <div class="navigation_menu">
                                        @include('default.nav_item.main', ['name' => 'main'])
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- end of the loop -->
        </div>
        <!----header end----->
        <!--===============PAGE CONTENT ==============-->
        <!--===============PAGE CONTENT ==============-->
        <div id="content" class="site-content ">
            <div style="width: 100%; background: var(--primary-color-one); padding: 20px 5px; text-align: center;"><a href="/darowizna" style="margin-bottom: 0px; color: #fff; font-size: 19px; font-weight: 500;">Przekaż darowiznę na rzecz Fundacji PRO IMMOBILARIA</a></div>

@yield('content')
            <div class="footer_area footer_three bg_dark_3" id="footer_contents">
                <!--===============spacing==============-->
                <div class="pd_top_70"></div>
                <!--===============spacing==============-->
                <div class="footer_widgets_wrap">
                    <div class="auto-container">
                        <div class="row">
                            <div class="col-12 col-md-4 mb-5 mb-lg-5 mb-xl-0">
                                <div class="footer_widgets wid_tit style_two">
                                    <div class="pd_top_20"></div>
                                    <div class="fo_wid_title">
                                        <h2>Dane kontaktowe</h2>
                                    </div>
                                </div>
                                <div class="footer_widgets clearfix navigation_foo light_color style_one">
                                    <div class="navigation_foo_box">
                                        <div class="navigation_foo_inner">
                                            <ul class="menu">
                                                <li style="color: var(--text-color-light);">{{getConstField('company_name')}}</li>
                                                <li style="color: var(--text-color-light);">{{getConstField('company_address')}}</li>
                                                <li style="color: var(--text-color-light);">{{getConstField('company_post_code')}} {{getConstField('company_city')}}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 mb-5 mb-lg-5 mb-xl-0">
                                <div class="footer_widgets wid_tit style_two">
                                    <!--===============spacing==============-->
                                    <div class="pd_top_50"></div>
                                    <!--===============spacing==============-->
                                    <div class="fo_wid_title">
                                        <h2></h2>
                                    </div>
                                </div>

                                <div class="footer_widgets clearfix navigation_foo light_color style_one">
                                    <div class="navigation_foo_box">
                                        <div class="navigation_foo_inner">

                                            <ul class="menu">
                                                <li style="color: var(--text-color-light); display: flex">Tel/fax: <a style="margin-left: 5px; margin-bottom: 0px" href="tel:{{str_replace(' ', '', getConstField('phone'))}}">{{getConstField('phone')}}</a></li>
                                                <li style="color: var(--text-color-light); display: flex">Tel: <a style="margin-left: 5px; margin-bottom: 0px" href="tel:{{str_replace(' ', '', getConstField('phone2'))}}">{{getConstField('phone2')}}</a></li>
                                                <li style="color: var(--text-color-light); display: flex">Email: <a style="margin-left: 5px; margin-bottom: 0px" href="mailto:{{getConstField('email')}}">{{getConstField('email')}}</a></li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 mb-5 mb-lg-5 mb-xl-0">
                                <div class="footer_widgets wid_tit style_two">
                                    <!--===============spacing==============-->
                                    <div class="pd_top_50"></div>
                                    <!--===============spacing==============-->
                                    <div class="fo_wid_title">
                                        <h2></h2>
                                    </div>
                                </div>

                                <div class="footer_widgets clearfix navigation_foo light_color style_one">
                                    <div class="navigation_foo_box">
                                        <div class="navigation_foo_inner">
                                            <ul class="menu">
                                                <li style="color: var(--text-color-light);">KRS: {{getConstField('company_krs')}}</li>
                                                <li style="color: var(--text-color-light);">REGON: {{getConstField('company_regon')}}</li>
                                                <li style="color: var(--text-color-light);">NIP: {{getConstField('company_nip')}}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--===============spacing==============-->
                <div class="pd_bottom_40"></div>
                <!--===============spacing==============-->
                <div class="bg_dark_1 copyright">
                    <!--===============spacing==============-->
                    <div class="pd_top_20"></div>
                    <!--===============spacing==============-->
                    <div class="auto-container">
                        <div class="row">
                            <div class="col-12 mb-2 mb-lg-0 mb-xl-0">
                                <div class="footer_copy_content color_white">
                                    <?php echo date("Y") ?> &copy; Wszelkie prawa zastrzeżone
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--===============spacing==============-->
                    <div class="pd_bottom_15"></div>
                    <!--===============spacing==============-->
                </div>
            </div>
        </div>
        <!--===============PAGE CONTENT END==============-->
        <!--===============PAGE CONTENT END==============-->
    </div>
    <!---==============footer start =================-->

    <!---==============footer end =================-->

    <!---==============mobile menu =================-->
    <div class="crt_mobile_menu">
        <div class="menu-backdrop"></div>
        <nav class="menu-box">
            <div class="close-btn"><i class="icon-close"></i></div>
            <div class="pb-3 logo_column">
                <div class="header_log_outer">
                    <div class="header_logo_box">
                        <a href="index.html" class="logo navbar-brand">
                            <img src="{{asset('images/logo.png')}}" class="logo_default">
                        </a>
                    </div>
                </div>
            </div>
            <div class="menu-outer">
                <!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header-->
            </div>
        </nav>
    </div>
    <!---==============mobile menu end =================-->
<div class="prgoress_indicator">
    <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
        <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98" />
    </svg>
</div>
<script src="https://code.jquery.com/jquery-3.6.1.min.js" integrity="sha256-o88AwQnZB+VDvE9tvIXrMQaPlFFSUTR+nldQm1LuPXQ=" crossorigin="anonymous"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/simplelightbox/2.10.3/simple-lightbox.jquery.min.js" integrity="sha512-iJCzEG+s9LeaFYGzCbDInUbnF03KbE1QV1LM983AW5EHLxrWQTQaZvQfAQgLFgfgoyozb1fhzhe/0jjyZPYbmQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script><script src="{{asset('js/frontend.js')}}"></script>
<script src="{{asset('js/main.min.js')}}"></script>
<script type='javascript' src='assets/js/jquery-3.6.0.min.js'></script>
<script type='text/javascript' src='{{asset('js/bootstrap.min.js')}}'></script>
<script type='text/javascript' src='{{asset('js/jquery.fancybox.js')}}'></script>
<script type='text/javascript' src='{{asset('js/jQuery.style.switcher.min.js')}}'></script>
<script type='text/javascript' src='{{asset('js/jquery.flexslider-min.js')}}'></script>
<script type='text/javascript' src='{{asset('js/color-scheme.js')}}'></script>
<script type='text/javascript' src='{{asset('js/owl.js')}}'></script>
<script type='text/javascript' src='{{asset('js/swiper.min.js')}}'></script>
<script type='text/javascript' src='{{asset('js/isotope.min.js')}}'></script>
<script type='text/javascript' src='{{asset('js/countdown.js')}}'></script>
<script type='text/javascript' src='{{asset('js/simpleParallax.min.js')}}'></script>
<script type='text/javascript' src='{{asset('js/appear.js')}}'></script>
<script type='text/javascript' src='{{asset('js/jquery.countTo.js')}}'></script>
<script type='text/javascript' src='{{asset('js/sharer.js')}}'></script>
<script type='text/javascript' src='{{asset('js/validation.js')}}'></script>
<!-- map script -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-CE0deH3Jhj6GN4YvdCFZS7DpbXexzGU"></script>
<script src="{{asset('js/gmaps.js')}}"></script>
<script src="{{asset('js/map-helper.js')}}"></script>
<!-- main-js -->
<script type='text/javascript' src='{{asset('js/creote-extension.js')}}'></script>

@stack('scripts.body.bottom')
</body>
</html>
