@extends('default.layout')
@section('content')
    @include('default.subheader', ['pageName' => $page->name])
    <div style="padding-top: 50px; padding-bottom: 50px" class="container">
        <div class="contact_map">
            {!! getConstField('google_map_iframe') !!}
        </div>
        <div class="contact_details" style="margin-top: 15px">
            <div class="row">
                <div class="col-12 col-md-4 mb-5 mb-lg-5 mb-xl-0">
                    <div class="footer_widgets wid_tit style_two">
                        <div class="pd_top_20"></div>
                        <div class="fo_wid_title">
                            <h2 style="color: var(--primary-color-two);">Dane kontaktowe:</h2>
                        </div>
                    </div>
                    <div class="footer_widgets clearfix navigation_foo light_color style_one">
                        <div class="navigation_foo_box">
                            <div class="navigation_foo_inner">
                                <ul class="menu">
                                    <li style="color: var(--primary-color-two);">{{getConstField('company_name')}}</li>
                                    <li style="color: var(--primary-color-two);">{{getConstField('company_address')}}</li>
                                    <li style="color: var(--primary-color-two);">{{getConstField('company_post_code')}} {{getConstField('company_city')}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-5 mb-lg-5 mb-xl-0">
                    <div class="footer_widgets wid_tit style_two">
                        <!--===============spacing==============-->
                        <div class="pd_top_50"></div>
                        <!--===============spacing==============-->
                        <div class="fo_wid_title">
                            <h2></h2>
                        </div>
                    </div>

                    <div class="footer_widgets clearfix navigation_foo light_color style_one">
                        <div class="navigation_foo_box">
                            <div class="navigation_foo_inner">

                                <ul class="menu">
                                    <li style="color: var(--primary-color-two); display: flex">Tel/fax: <a style="margin-left: 5px; margin-bottom: 0px;color: var(--primary-color-two);" href="tel:{{str_replace(' ', '', getConstField('phone'))}}">{{getConstField('phone')}}</a></li>
                                    <li style="color: var(--primary-color-two); display: flex">Tel: <a style="margin-left: 5px;     margin-bottom: 0px;color: var(--primary-color-two);" href="tel:{{str_replace(' ', '', getConstField('phone2'))}}">{{getConstField('phone2')}}</a></li>
                                    <li style="color: var(--primary-color-two); display: flex">Email: <a style="margin-left: 5px;     margin-bottom: 0px;color: var(--primary-color-two);" href="mailto:{{getConstField('email')}}">{{getConstField('email')}}</a></li>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-5 mb-lg-5 mb-xl-0">
                    <div class="footer_widgets wid_tit style_two">
                        <!--===============spacing==============-->
                        <div class="pd_top_50"></div>
                        <!--===============spacing==============-->
                        <div class="fo_wid_title">
                            <h2></h2>
                        </div>
                    </div>

                    <div class="footer_widgets clearfix navigation_foo light_color style_one">
                        <div class="navigation_foo_box">
                            <div class="navigation_foo_inner">
                                <ul class="menu">
                                    <li style="color: var(--primary-color-two);">KRS: {{getConstField('company_krs')}}</li>
                                    <li style="color: var(--primary-color-two);">REGON: {{getConstField('company_regon')}}</li>
                                    <li style="color: var(--primary-color-two);">NIP: {{getConstField('company_nip')}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form_content" style="padding-top: 15px">
            @include('default.form.contact_form')
        </div>
    </div>
@endsection
