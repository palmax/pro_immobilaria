@extends('default.layout')
@section('content')
    <div class="page_header_default style_one ">
        <div class="parallax_cover">
            <img src="{{renderImage($page->galleryCover(), 600, 600, 'resize')}}"  alt="bg_image" class="cover-parallax">
        </div>
        <div class="page_header_content">
            <div class="auto-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="banner_title_inner">
                            <div class="title_page">
                                {{$page->name}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="breadcrumbs creote">
                            <ul class="breadcrumb m-auto">
                                <li><a href="/">Strona główna</a> </li>
                                <li class="active">{{$page->name}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="container" style="padding-top: 50px; padding-bottom: 50px">
    <div>{!! $fields->text1 ?? '- none -' !!}</div>
</div>

@endsection
