@extends('default.layout')
@section('content')
{{--{!! getPhoneLink('phone', 'phone', '<b>icon</b> ') !!}--}}
{{--{!! getEmailLink('email', 'email',  '<b>icon</b> ') !!}--}}
{{--    <span style="display: block">{!! getAddressString() !!}</span>--}}
{{--    <span style="display: block">{!! getFooterCreator() !!}</span>--}}

{{--    @include('default.rotator.base', ['id_rotator' => $fields->rotator, 'type' => 'main']) --}}

<section class="service-section">
    <div class="row home_banner">
        <div class="col-12 col-md-3  home_banner--single">
            <img src="{{asset('images/1.jpg')}}" />
        </div>
        <div class="col-12 col-md-3  home_banner--single">
            <img src="{{asset('images/2.jpg')}}" />
        </div>
        <div class="col-12 col-md-3  home_banner--single">
            <img src="{{asset('images/3.jpg')}}" />
        </div>
        <div class="col-12 col-md-3  home_banner--single">
            <img src="{{asset('images/4.jpg')}}" />
        </div>
        <div class="col-12 col-md-3  home_banner--single">
            <img src="{{asset('images/5.jpg')}}" />
        </div>
        <div class="col-12 col-md-3  home_banner--single">
            <img src="{{asset('images/6.jpg')}}" />
        </div>
        <div class="col-12 col-md-3  home_banner--single">
            <img src="{{asset('images/7.jpg')}}" />
        </div>
        <div class="col-12 col-md-3  home_banner--single">
            <img src="{{asset('images/8.jpg')}}" />
        </div>
        <div class="col-12 col-md-3 home_banner--single">
            <img src="{{asset('images/9.jpg')}}" />
        </div>
        <div class="col-12 col-md-3 home_banner--single">
            <img src="{{asset('images/10.jpg')}}" />
        </div>
        <div class="col-12 col-md-3 home_banner--single">
            <img src="{{asset('images/11.jpg')}}" />
        </div>
        <div class="col-12 col-md-3 home_banner--single">
            <img src="{{asset('images/12.jpg')}}" />
        </div>
    </div>
</section>

@endsection
