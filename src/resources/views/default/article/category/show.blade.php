@extends('default.layout')
@section('content')
    <div class="page_header_default style_one ">
        <div class="parallax_cover">
            <img src="{{asset('images/forest_bg.jpg')}}"  alt="bg_image" class="cover-parallax">
        </div>
        <div class="page_header_content">
            <div class="auto-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="banner_title_inner">
                            <div class="title_page">
                                Aktualności - {{$item->title}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="breadcrumbs creote">
                            <ul class="breadcrumb m-auto">
                                <li><a href="/">Strona główna</a> </li>
                                <li class="active">{{$item->title}}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="padding-top: 50px; padding-bottom: 50px" class="container">
        <div class="row">
            @foreach($items as $item)
                <div class="col-4">
                        <a href="{{route('article.show.'.$item->id)}}">
                            <img style="margin-bottom: 15px"  src="{{renderImage($item->galleryCover(), 600, 600, 'resize')}}" />
                            <h5 style="text-align: center" >{{$item->title}}</h5>
                        </a>
                    </div>
            @endforeach
        </div>
    </div>
@endsection
