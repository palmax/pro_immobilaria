@extends('default.layout')
@section('content')
    <div class="page_header_default style_one ">
        <div class="parallax_cover">
            <img src="{{renderImage($item->galleryCover(), 600, 600, 'resize')}}"  alt="bg_image" class="cover-parallax">
        </div>
        <div class="page_header_content">
            <div class="auto-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="banner_title_inner">
                            <div class="title_page">
                                {{$item->title}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="padding-top: 50px; padding-bottom: 50px" class="container">
        <div  style="padding-bottom: 10px" class="meta-data">
            <h4>{{$item->title}}</h4>
            <p style="font-size: 14px; font-weight: 500; margin-top: 5px; margin-left: 2px">Data dodania: <span style="font-weight: 400">{{$item->created_at->format('d-m-Y')}}</span></p>
        </div>
        <p>{!! $item->lead !!}</p>
        <p>{!! $item->text !!}</p>
        <div style="padding-top: 25px" class="gallery">
            @foreach($item->gallery->items as $item)
                @if ($item->type !== 'cover')
                <div class="col-lg-4">
                    <img style="width: 100%;" src="{{renderImage($item->url,  'fit')}}" alt="">
                </div>
                @endif
            @endforeach
        </div>
    </div>

@endsection
