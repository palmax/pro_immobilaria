<div>
    <div class="page_header_default style_one ">
        <div class="parallax_cover">
            <img src="{{asset('images/forest_bg.jpg')}}"  alt="bg_image" class="cover-parallax">
        </div>
        <div class="page_header_content">
            <div class="auto-container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="banner_title_inner">
                            <div class="title_page">
                                {{$pageName}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
